import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaguesComponent } from './pages/leagues/leagues.component';
import { NotFoundErrorPageComponent } from './pages/not-found-error-page/not-found-error-page.component';
import { TeamsComponent } from './pages/teams/teams.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: WelcomeComponent
  },
  {
    path: 'teams/:id',
    component: TeamsComponent
  },
  {
    path: 'leagues/:id',
    component: LeaguesComponent
  },
  {
    path:'404',
    component:NotFoundErrorPageComponent
  },
  {
    path:'**',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
