import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API } from '@core/models/api/api';
import { ErrorResponse } from '@core/models/api/error-response';
import { HttpResponse } from '@core/models/api/http-response';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private readonly _http: HttpClient) { }

  // ---------------------  leagues ------------------

  /**
   * Make Http request to get the suggestion list for leagues
   * @description get leagues by suggestion name
   * @param {string} query - query the league name
   * @returns  {HttpResponse} leagues array as Observable
   */

   getSuggestions (query: string): Observable<HttpResponse | ErrorResponse> {
     const url = `${API.core}/${API.leagues}/${API.search}?query=${query}`;
    return this._http.get<HttpResponse | ErrorResponse>(url);
   }

  /**
   * Make Http request to get a league by id
   * @description get a league by id
   * @param {string} id - league id
   * @returns  {HttpResponse} league object as Observable
   */

  getLeagueById (id: string): Observable<HttpResponse | ErrorResponse> {
    const url = `${API.core}/${API.leagues}/${id}`;
   return this._http.get<HttpResponse | ErrorResponse>(url);
  }

  //---------------------- teams ----------------------------

  /**
   * Make Http request to get a team by id
   * @description get a team by id
   * @param {string} id - league id
   * @returns  {HttpResponse} team object  as Observable
   */

  getTeamById (id: string): Observable<HttpResponse | ErrorResponse> {
    const url = `${API.core}/${API.teams}/${id}`;
   return this._http.get<HttpResponse | ErrorResponse>(url);
  }
}
