import { Player } from "./player";

export interface Team {
    _id: string;
    name: string;
    thumbnail: string;
    players: Player [];
}
