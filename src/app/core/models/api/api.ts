import { environment } from "src/environments/environment";
/**
 * API to allow operators to manage foreign exchanges
 *
 * @readonly
 * @class API
 */

export class API {

    static apiPath = environment.apiHost;

    /**
   * @description Core path
   */
  static get core(): string {
    return API.apiPath ;
  }

    static get suggestions(): string {
        return 'suggestions';
    }

    static get leagues(): string {
        return 'leagues';
    }

    static get teams(): string {
        return 'teams';
    }

    static get players(): string {
        return 'players';
    }

    static get search(): string {
        return 'search';
    }
}
