import { Team } from "./team";

export interface League {
    _id: string;
     name: string;
     sports: string;
     teams: Team [];
}


