import { League } from "./league";
import { Player } from "./player";
import { Team } from "./team";

export interface HttpResponse {
    message: string;
    data: Player | Team | League | League [] | Player [] | Team [] | Player [];
}
