export interface Player {
    _id: string;
     name: string;
     position: string;
     thumbnail: string;
     signin : Signin;
     born :Date;
}

export interface Signin {
    amount: number;
    currency: string;
}
