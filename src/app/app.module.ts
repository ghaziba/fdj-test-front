import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotFoundErrorPageComponent } from './pages/not-found-error-page/not-found-error-page.component';
import { TeamsComponent } from './pages/teams/teams.component';
import { LeaguesComponent } from './pages/leagues/leagues.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';



@NgModule({
  declarations: [
    AppComponent,
    NotFoundErrorPageComponent,
    TeamsComponent,
    LeaguesComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
