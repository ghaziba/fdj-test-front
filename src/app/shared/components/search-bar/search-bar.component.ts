import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpResponse } from '@core/models/api/http-response';
import { League } from '@core/models/api/league';
import { ApiService } from '@core/services/api/api.service';

import { map } from 'rxjs/operators';

@Component({
  selector: 'fdj-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  searchQuery: string;
  suggestionLeaguesList: League[] = [];

  constructor(
    private readonly _api: ApiService,
    private readonly _router: Router
    ) { }

  ngOnInit(): void {
  }

  searchForLeague() {
    this._getSuggestion(this.searchQuery);
  }

  navigateToLeague(league:League) {
    this._router.navigate(['leagues', league._id]);
    this.searchQuery = league.name;
    this.suggestionLeaguesList = [];
  }

  clearSearchBox() {
    this.searchQuery ='';
    this.suggestionLeaguesList = []
  }

  private _getSuggestion(query: string) {
      this._api.getSuggestions(query).pipe(
        map((leaguesData:HttpResponse)=> {
          return leaguesData.data;
        })
      ).subscribe(
        (formattedLeagues: League[]) => {
          this.suggestionLeaguesList = formattedLeagues;
        }
      );
  }

}
