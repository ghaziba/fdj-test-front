import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { HighlightDirective } from './directives/highlight.directive';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ShortNumberPipe } from './pipes/short-number.pipe';




@NgModule({
  declarations: [
    SearchBarComponent,
    HighlightDirective,
    ShortNumberPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    RouterModule
  ],
  exports: [
    SearchBarComponent,
    HighlightDirective,
    RouterModule,
    FormsModule,
    ShortNumberPipe
  ]
})
export class SharedModule { }
