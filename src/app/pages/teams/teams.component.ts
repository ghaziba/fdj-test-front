import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@core/models/api/http-response';
import { Player } from '@core/models/api/player';
import { Team } from '@core/models/api/team';
import { ApiService } from '@core/services/api/api.service';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'fdj-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {

  teamData : Team;
  players: Player [];

  private _routeParams$: Subscription;

  constructor(
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _api: ApiService
    ) { }

  ngOnInit(): void {
    this._routeParams$ = this._activatedRoute.params.subscribe(
      (params) => {
        this._getTeamById(params.id);
      }
    );
  }

  ngOnDestroy(): void {
    this._routeParams$.unsubscribe();
  }

  private _getTeamById (id:string) {
    this._api.getTeamById(id).pipe(
      map((teamData:HttpResponse)=> {
        return teamData.data;
      })
    ).subscribe(
      (formattedTeam: Team) => {
        this.teamData = formattedTeam;
        this.players = this.teamData.players;
      }
    )
  }

}
