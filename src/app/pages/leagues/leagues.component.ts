import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@core/models/api/http-response';
import { League } from '@core/models/api/league';
import { Team } from '@core/models/api/team';
import { ApiService } from '@core/services/api/api.service';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'fdj-leagues',
  templateUrl: './leagues.component.html',
  styleUrls: ['./leagues.component.scss']
})
export class LeaguesComponent implements OnInit ,OnDestroy {

  leagueData : League;
  teams: Team [];
  
  private _routeParams$: Subscription;

  constructor(
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _api: ApiService
    ) { }

  ngOnInit(): void {
    this._routeParams$ = this._activatedRoute.params.subscribe(
      (params) => {
        this._getLeagueById(params.id);
      }
    );
  }

  ngOnDestroy(): void {
    this._routeParams$.unsubscribe();
  }

  private _getLeagueById (id:string) {
    this._api.getLeagueById(id).pipe(
      map((leagueData:HttpResponse)=> {
        return leagueData.data;
      })
    ).subscribe(
      (formattedLeague: League) => {
        this.leagueData = formattedLeague;
        this.teams = this.leagueData.teams;
      }
    )
  }

}
