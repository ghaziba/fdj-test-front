## Install dependencies

Run `npm install`
## Development server

Run `ng serve` or `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Documentation

Run `npm run doc:build` to generate the app documentation folder

Run `npm run doc:serve` to serve the documentation. Navigate to `http://localhost:8080/`

## screenshots

![IMAGE_DESCRIPTION](https://gitlab.com/ghaziba/fdj-test-front/-/raw/master/src/assets/screen-shots/fdj-test-1.png)
![IMAGE_DESCRIPTION](https://gitlab.com/ghaziba/fdj-test-front/-/raw/master/src/assets/screen-shots/fdj-test-2.png)
![IMAGE_DESCRIPTION](https://gitlab.com/ghaziba/fdj-test-front/-/raw/master/src/assets/screen-shots/fdj-test-3.png)
![IMAGE_DESCRIPTION](https://gitlab.com/ghaziba/fdj-test-front/-/raw/master/src/assets/screen-shots/fdj-front-doc.png)
